<?php 
include 'inc/head.php';

 ?>

</head>
<body>
<?php include 'inc/topo.php'; ?>

<main class="container">
	<h1>Contato</h1>
		<?php
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['EnviaContato'])):
	          unset($post['EnviaContato']);
			if (in_array("", $post)):
				echo '<script> pop_up_hacker("Erro: Preencha todos os campos."); </script>';
		    else:         
		        include('contato-envia.php');
	        endif;
          endif;
          ?> 
<div class="form">
	<form enctype="multipart/form-data" method="post">
		<label for="nome">Nome: * </label>
		<input  type="text" name="nome" value="<?php
		if (isset($post['nome'])): echo $post['nome'];
		endif;
		?>" id="nome"/>
		<label for="email">E-mail: * </label>
		<input type="email" name="email" value="<?php
		if (isset($post['email'])): echo $post['email'];
		endif;
		?>" id="email" required/>
		<label for="tel">Telefone: * </label>
		<input type="text" name="telefone" value="<?php
		if (isset($post['telefone'])): echo $post['telefone'];
		endif;
		?>" id="tel"/>
		<label>Mensagem: * </label>
		<textarea name="mensagem" rows="5" ><?php
		if (isset($post['mensagem'])): echo $post['mensagem'];
		endif;
		?></textarea>
		<span class="bt-submit">
		<input type="submit" name="EnviaContato" value="Enviar" class="ir" />
		</span>
	</form>
</div>

</main>

<?php 
	include 'inc/footer.php';
 ?>
</body>