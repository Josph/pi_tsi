-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Dez-2019 às 06:22
-- Versão do servidor: 10.1.40-MariaDB
-- versão do PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

CREATE TABLE `banner` (
  `ban_id` int(11) NOT NULL,
  `ban_nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ban_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ban_file` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ban_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `banner`
--

INSERT INTO `banner` (`ban_id`, `ban_nome`, `ban_link`, `ban_file`, `ban_status`) VALUES
(1, 'Black WEEK', 'contato', 'black-week-1575523294.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `cat_id` int(11) NOT NULL,
  `cat_parent` int(11) DEFAULT NULL,
  `cat_nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_descricao` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_conteudo` text CHARACTER SET utf8 NOT NULL,
  `cat_url` varchar(255) NOT NULL,
  `cat_file` varchar(255) NOT NULL,
  `cat_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`cat_id`, `cat_parent`, `cat_nome`, `cat_descricao`, `cat_conteudo`, `cat_url`, `cat_file`, `cat_status`) VALUES
(1, NULL, 'PlayStation', 'Playstation', 'LanÃ§ado em 3 de dezembro de 1994, no JapÃ£o, e em 29 de setembro de 1995, nos Estados Unidos. Desde o seu lanÃ§amento atÃ© 2006 (quando sua produÃ§Ã£o foi extinta), o PlayStation vendeu mais de 103 milhÃµes de unidades. Inicialmente o Play Station (separados mesmo) seria um leitor de CD-ROM para o Super Nintendo Entertainment System, a ser produzido em conjunto com a Nintendo. Durante as negociaÃ§Ãµes para o lanÃ§amento, o acordo entre as empresas foi rompido. EntÃ£o o engenheiro Ken Kutaragi convenceu os executivos da Sony a continuar com o projeto. Foi aÃ­ que nasceu a divisÃ£o Sony Computer Entertainment (atualmente Sony Interactive Entertainment), com o objetivo de implementar no mercado um novo console de videogame: o PlayStation. Mal sabia a Nintendo que nascia ali o seu maior rival no mundo do entretenimento eletrÃ´nico, que superou atÃ© mesmo a Sega.', 'playstation', 'playstation-1575251146.png', 1),
(2, NULL, 'Xbox', 'Xbox', 'O Xbox Ã© um console de vÃ­deo game produzido pela Microsoft. Foi lanÃ§ado em 15 de novembro de 2001 na AmÃ©rica do Norte, 22 de fevereiro de 2002 no JapÃ£o, e 14 de MarÃ§o de 2002 na AustrÃ¡lia e Europa. Foi a primeira incursÃ£o da Microsoft no mercado de vÃ­deo games. Como parte da sexta-geraÃ§Ã£o de jogos, o Xbox competiu com Sony PlayStation 2, Sega Dreamcast (que parou as vendas americanas antes que o Xbox fosse colocado Ã  venda) e o Nintendo GameCube. O Xbox foi o primeiro console oferecido por uma empresa norte-americana apÃ³s o Atari Jaguar parar as vendas em 1996. O nome Xbox foi derivado a partir de uma contraÃ§Ã£o da caixa \"DirectX Box\", uma referÃªncia a Microsoft.', 'xbox', 'xbox-1575251598.png', 1),
(3, NULL, 'PC', 'PC.', 'PC Gamer Ã© uma revista fundada na GrÃ£ Bretanha em 1993 focada em videojogos para computadores pessoais publicados mensalmente pela Future Publishing. A revista possui vÃ¡rias ediÃ§Ãµes regionais, sendo que os Estados Unidos e o Reino Unido sÃ£o os que tem as ediÃ§Ãµes mais vendidas em seus respectivos paÃ­ses.', 'pc', 'pc-1575251752.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `pedi_id` int(11) NOT NULL,
  `pedi_prod_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `pedi_qtd` int(11) NOT NULL,
  `pedi_data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `prod_id` int(11) NOT NULL,
  `cat_parent` int(11) DEFAULT NULL,
  `prod_nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_preco` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_descricao` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_conteudo` text CHARACTER SET utf8 NOT NULL,
  `prod_status` int(11) NOT NULL DEFAULT '1',
  `prod_file` varchar(250) CHARACTER SET utf8 NOT NULL,
  `prod_url` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `senha` varchar(40) NOT NULL,
  `palavra` varchar(40) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `email`, `nome`, `senha`, `palavra`, `data_cadastro`) VALUES
(1, 'admin@admin.com', 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '0000-00-00 00:00:00'),
(2, 'pedro@pedro.com.br', 'teste', '698dc19d489c4e4db73e28a713eab07b', 'teste', '0000-00-00 00:00:00'),
(3, 'admin@admin.comkjh', 'Pedro Henrique Firmino dos Santos', '2c51e77440743719e3957aa13047d336', 'qqq', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`ban_id`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`pedi_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `ban_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `pedi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
