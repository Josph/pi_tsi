<?php 
include 'inc/head.php';

 ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" type="text/css">

</head>
<body>
<?php include 'inc/topo.php'; ?>
<div class="theme-default">
	<div id="slider" class="nivoSlider">
<?php 
		$Read->ExeRead("banner", "WHERE ban_status = 1 ORDER BY ban_nome ASC");
		if($Read->getResult()):
		foreach ($Read->getResult() as $ban):
		extract($ban);	
 ?>
		<a href="<?=$url.$ban_link?>" title="<?=$ban_nome?>">
			<img src="<?=$url?>painel/uploads/banner/<?=$ban_file?>" alt="<?=$ban_nome?>">
		</a>
<?php 
		endforeach;
		endif;
 ?>
	</div>
</div>

<main class="container">
	<h1 class="text-center">Todos os principais titulos de de PC e console você encontra na Kstation</h1>

	<div class="conteudo-home">
		<div class="img-home">
			<img src="imagens/ps4.jpg" alt="PS4">
		</div>
		<div class="info">
			<h2>PlayStation</h2>
			<p>Lançado em 3 de dezembro de 1994, no Japão, e em 29 de setembro de 1995, nos Estados Unidos. Desde o seu lançamento até 2006 (quando sua produção foi extinta), o PlayStation vendeu mais de 103 milhões de unidades. Inicialmente o Play Station (separados mesmo) seria um leitor de CD-ROM para o Super Nintendo Entertainment System, a ser produzido em conjunto com a Nintendo. Durante as negociações para o lançamento, o acordo entre as empresas foi rompido. Então o engenheiro Ken Kutaragi convenceu os executivos da Sony a continuar com o projeto. Foi aí que nasceu a divisão Sony Computer Entertainment (atualmente Sony Interactive Entertainment), com o objetivo de implementar no mercado um novo console de videogame: o PlayStation. Mal sabia a Nintendo que nascia ali o seu maior rival no mundo do entretenimento eletrônico, que superou até mesmo a Sega.</p>
		</div>
	</div>
	<?php 
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_url = 'playstation' ORDER BY cat_nome ASC");
		$cat = $Read->getResult();
		$categ = $cat[0]['cat_url'];
		$catId = $cat[0]['cat_id'];
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent = $catId ORDER BY cat_nome ASC LIMIT 5");
		if ($Read->getResult()):
	 ?>
	<ul class="thumbnails">
			<?php
				foreach ($Read->getResult() as $cat):
				extract($cat);
				?>

				<li> 
					<a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>">
					<img src="<?=$url?>painel/uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>" title="<?=$cat_nome?>"/></a>
					<h2><a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>"><?=$cat_nome?></a></h2>
				</li>
				<?php
				endforeach;
				?>
	</ul>
	<?php 
		endif;
	 ?>
	<div class="conteudo-home home-right">
		<div class="img-home">
			<img src="imagens/xbox.jpg" alt="PS4">
		</div>
		<div class="info">
			<h2>Xbox</h2>
			<p>O Xbox é um console de vídeo game produzido pela Microsoft. Foi lançado em 15 de novembro de 2001 na América do Norte, 22 de fevereiro de 2002 no Japão, e 14 de Março de 2002 na Austrália e Europa. Foi a primeira incursão da Microsoft no mercado de vídeo games. Como parte da sexta-geração de jogos, o Xbox competiu com Sony PlayStation 2, Sega Dreamcast (que parou as vendas americanas antes que o Xbox fosse colocado à venda) e o Nintendo GameCube. O Xbox foi o primeiro console oferecido por uma empresa norte-americana após o Atari Jaguar parar as vendas em 1996. O nome Xbox foi derivado a partir de uma contração da caixa "DirectX Box", uma referência a Microsoft.</p>
		</div>
	</div>
	<?php 
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_url = 'xbox' ORDER BY cat_nome ASC");
		$cat = $Read->getResult();
		$categ = $cat[0]['cat_url'];
		$catId = $cat[0]['cat_id'];
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent = $catId ORDER BY cat_nome ASC LIMIT 5");
		if ($Read->getResult()):
	 ?>
	<ul class="thumbnails">
			<?php
				foreach ($Read->getResult() as $cat):
				extract($cat);
				?>

				<li> 
					<a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>">
					<img src="<?=$url?>painel/uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>" title="<?=$cat_nome?>"/></a>
					<h2><a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>"><?=$cat_nome?></a></h2>
				</li>
				<?php
				endforeach;
				?>
	</ul>
	<?php 
		endif;
	 ?>
	<div class="conteudo-home">
		<div class="img-home">
			<img src="imagens/teclado-gamer.jpg" alt="PS4">
		</div>
		<div class="info">
			<h2>PC</h2>
			<p>PC Gamer é uma revista fundada na Grã Bretanha em 1993 focada em videojogos para computadores pessoais publicados mensalmente pela Future Publishing. A revista possui várias edições regionais, sendo que os Estados Unidos e o Reino Unido são os que tem as edições mais vendidas em seus respectivos países.</p>
		</div>
	</div>
	<?php 
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_url = 'pc' ORDER BY cat_nome ASC");
		$cat = $Read->getResult();
		$categ = $cat[0]['cat_url'];
		$catId = $cat[0]['cat_id'];
		$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent = $catId ORDER BY cat_nome ASC LIMIT 5");
		if ($Read->getResult()):
	 ?>
	<ul class="thumbnails">
			<?php
				foreach ($Read->getResult() as $cat):
				extract($cat);
				?>

				<li> 
					<a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>">
					<img src="<?=$url?>painel/uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>" title="<?=$cat_nome?>"/></a>
					<h2><a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>"><?=$cat_nome?></a></h2>
				</li>
				<?php
				endforeach;
				?>
	</ul>
	<?php 
		endif;
	 ?>

</main>

<?php 
	include 'inc/footer.php';
 ?>

<script  src="js/jquery-1.7.2.min.js"></script>
<script  src="nivo/jquery.nivo.slider.js"></script>
<script>
$(window).load(function() {
$('#slider').nivoSlider({
	effect: 'fade',
	pauseTime: 3000,
	directionNav: true,
	controlNav: false
});
});
</script>
</body>