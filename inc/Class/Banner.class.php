<?php 
require_once('Create.class.php');
require_once('Read.class.php');
require_once('Update.class.php');

class Banner{


  	private $Dados;
  	private $Result;
  	private $Id;
  	Public $NomeBanner;


	public function ExeCreate(array $Dados) {
		$this->Dados = $Dados;
		$this->CheckBanner();

		if($this->NomeBanner):
			$this->Url();
			$this->Imagem();
			$this->Cadastarar();
		endif;
	}

	public function ExeUpdate(array $Dados) { 
		$this->Dados = $Dados;
		
		$this->Id = $this->Dados['ban_id'];
    	unset($this->Dados['ban_id']);

		$this->CheckBanner();

		if($this->NomeBanner):
			$this->CheckFileUpdate(); 
			$this->Update();
		endif;
	}

	private function UpdateFile(){
		$readFile = new Read;
		$readFile->ExeRead("banner", "WHERE ban_id = {$this->Id}");
		$file = $readFile->getResult();
		if ($readFile->getResult()):
			$delFile = $file[0]['ban_file'];
			if (file_exists("uploads/banner/{$delFile}")):
		  		unlink("uploads/banner/{$delFile}");
			endif;
			$this->Url();
			$this->Imagem();
		else:
			$this->Result = false;
		endif;
	}

	private function Cadastarar() {
		$Create = new Create();
		$Create->ExeCreate("banner", $this->Dados); 
		$this->Result = $Create->getResult();
	}

	private function Update() {
		$Update = new Update();
		$Update->ExeUpdate("banner", $this->Dados, "WHERE ban_id = {$this->Id} "); 
		$this->Result = $Update->getResult();
	}

    public function getResult() {
        return $this->Result;
    }

	private function CheckFileUpdate() {
        if (isset($this->Dados['ban_file'])):
      		$this->UpdateFile();
    	endif;
	}

	private function CheckBanner() { 
		$Read = new Read(); 
		$WHERE = (!empty($this->Id) ? "ban_id != {$this->Id} AND" : null);
		$Read->ExeRead("banner", "WHERE {$WHERE} ban_nome = '{$this->Dados['ban_nome']}'");
		if (!$Read->getResult()):
			$this->NomeBanner = true;
		else:
			$this->NomeBanner = false;
		endif; 
	}

	private function Url(){
		$this->Dados['ban_url'] = $this->Dados['ban_nome'];
		$str = $this->Dados['ban_url'];
	    $str = strtolower(utf8_decode($str)); $i=1;
	    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
	    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
	    while($i>0) $str = str_replace('--','-',$str,$i);
	    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
	    $this->Dados['ban_url'] = $str;
	}

	private function Imagem() {
		$extensao = strtolower(end(explode('.', $_FILES['ban_file']['name'])));
	 	$nome = $this->Dados['ban_url'].'-'.time() . '.' . $extensao;
		move_uploaded_file($this->Dados['ban_file'], "uploads/banner/" . $nome);
		$this->Dados['ban_file'] = $nome;
		unset($this->Dados['ban_url']); 
  

	}




}