<?php 
require_once('Create.class.php');
require_once('Read.class.php');
require_once('Update.class.php'); 
class Categoria{


  	private $Dados;
  	private $Result;
  	private $Id;
  	Public $NomeCat;


	public function ExeCreate(array $Dados) {
		$this->Dados = $Dados;
		$this->CheckCat();

		if($this->NomeCat):
			$this->Url();
			$this->Imagem();
			$this->Cadastarar();
		endif;
	}

	public function ExeUpdate(array $Dados) { 
		$this->Dados = $Dados;
		
		$this->Id = $this->Dados['cat_id'];
    	unset($this->Dados['cat_id']);

    	if (!isset($this->Dados['cat_parent'])):    		
    		unset($this->Dados['cat_parent']);
    	endif;

		$this->CheckCat();

		if($this->NomeCat):
			$this->Url();
			$this->CheckFileUpdate();
			$this->Update();
		endif;
	}

	private function UpdateFile(){
		$readFile = new Read;
		$readFile->ExeRead("categoria", "WHERE cat_id = {$this->Id}");
		$file = $readFile->getResult();
		if ($readFile->getResult()):
			$delFile = $file[0]['cat_file'];
			if (file_exists("uploads/categoria/{$delFile}")):
		  		unlink("uploads/categoria/{$delFile}");
			endif;
			$this->Imagem();
		else:
			$this->Result = false;
		endif; 
	}

	private function Cadastarar() {
		$Create = new Create();
		$Create->ExeCreate("categoria", $this->Dados); 
		$this->Result = $Create->getResult();
	}

	private function Update() {
		$Update = new Update();
		$Update->ExeUpdate("categoria", $this->Dados, "WHERE cat_id = {$this->Id} "); 
		$this->Result = $Update->getResult();
	}

    public function getResult() {
        return $this->Result;
    }

	private function CheckFileUpdate() {
        if (isset($this->Dados['cat_file'])):
      		$this->UpdateFile();
    	endif;
	}

	private function CheckCat() {
		$Read = new Read(); 
		$WHERE = (!empty($this->Id) ? "cat_id != {$this->Id} AND" : null);
		$Read->ExeRead("categoria", "WHERE {$WHERE} cat_nome = '{$this->Dados['cat_nome']}' && cat_nome = NULL");
		if (!$Read->getResult()):
			$this->NomeCat = true;
		else:
			$this->NomeCat = false;
		endif; 
	}

	private function Url(){
		$this->Dados['cat_url'] = $this->Dados['cat_nome'];
		$str = $this->Dados['cat_url'];
	    $str = strtolower(utf8_decode($str)); $i=1;
	    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
	    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
	    while($i>0) $str = str_replace('--','-',$str,$i);
	    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
	    $this->Dados['cat_url'] = $str;
	}

	private function Imagem() {
		$extensao = strtolower(end(explode('.', $_FILES['cat_file']['name'])));
	 	$nome = $this->Dados['cat_url'].'-'.time() . '.' . $extensao;
		move_uploaded_file($this->Dados['cat_file'], "uploads/categoria/" . $nome);
		$this->Dados['cat_file'] = $nome;
  

	}




}