<?php 
class Conn {

	private $Host = "localhost";
    private $User = "root";
    private $Pass = "";
    private $Dbsa = "crud";

    private function Conectar() {
		try{
		$pdo = new PDO("mysql:dbname=".$this->Dbsa.";port=3307; host=".$this->Host."",$this->User, $this->Pass);

		} catch (PDOException $e){
		    echo "Erro com banco de dados: ".$e->getMessage();
		}
		catch(Exception $e){
		    echo "Erro generico: ".$e->getMessage();
		}

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;

    }

    protected function getConn() {
        return $this->Conectar();
    }


} 