<?php
require_once('Conn.class.php'); 
class Create extends Conn {

    private $Tabela;
    private $Dados;
    private $Create;
    private $Conn;
    private $Result;


    public function ExeCreate($Tabela, array $Dados) {
        $this->Tabela = (string) $Tabela;
        $this->Dados = $Dados;

        $this->getSyntax();
        $this->Execute();
    }

    private function getSyntax() {
        $coluna = implode(', ', array_keys($this->Dados));
        $valor = '"' .implode('", "', array_values($this->Dados)).'"';
        $this->Create = "INSERT INTO {$this->Tabela} ({$coluna}) VALUES ({$valor})";
    }

    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Create = $this->Conn->prepare($this->Create);
    }


	private function Execute() {
		$this->Connect();
		try {
            $this->Create->execute($this->Dados);
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = false;
            echo "<b>Erro ao cadastrar: </b> {$e->getMessage()}";
        }
	}

    public function getResult() {
        return $this->Result;
    }

}