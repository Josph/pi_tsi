<?php
require_once('Conn.class.php'); 
class Delete extends Conn {
	private $DeleteFrom;
    private $Result;
    private $Delete;
    private $Conn;

    public function ExeDelete($Tabela, $Termos) {
        $this->DeleteFrom = "DELETE FROM {$Tabela} {$Termos}";
        $this->Execute();
    }


    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Delete = $this->Conn->prepare($this->DeleteFrom);
    }

    private function Execute() {
        $this->Connect();
        try { 
            $this->Delete->execute();
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = null;
            echo "<b>Erro ao ler: </b> {$e->getMessage()}";
        }
    }


    public function getResult() {
        return $this->Result;
    }


}