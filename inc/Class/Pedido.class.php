<?php 
require_once('Create.class.php');
require_once('Read.class.php');
require_once('Update.class.php');
class Pedido{


  	private $Dados;
  	private $Result;


	public function ExeCreate(array $Dados) {
		$this->Dados = $Dados;

		$this->Cadastarar();
	}


	private function Cadastarar() {
		$Create = new Create();
		$Create->ExeCreate("pedido", $this->Dados);
		$this->Result = $Create->getResult();
	}

    public function getResult() {
        return $this->Result;
    }


}