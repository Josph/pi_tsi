<?php 
require_once('Create.class.php');
require_once('Read.class.php');
require_once('Update.class.php');
class Produto{


  	private $Dados;
  	private $Result;
  	private $Id;
  	Public $NomeProd;


	public function ExeCreate(array $Dados) {
		$this->Dados = $Dados;
		$this->CheckProd();

		if($this->NomeProd):
			$this->Url();
			$this->Imagem();
			$this->Cadastarar();
		endif;
	}

	public function ExeUpdate(array $Dados) { 
		$this->Dados = $Dados;
		
		$this->Id = $this->Dados['prod_id'];
    	unset($this->Dados['prod_id']);

    	if (!isset($this->Dados['cat_parent'])):    		
    		unset($this->Dados['cat_parent']); 
    	endif;

		$this->CheckProd();

		if($this->NomeProd):
			$this->Url();
			$this->CheckFileUpdate(); 
			$this->Update();
		endif;
	}

	private function UpdateFile(){
		$readFile = new Read;
		$readFile->ExeRead("produto", "WHERE prod_id = {$this->Id}");
		$file = $readFile->getResult();
		if ($readFile->getResult()):
			$delFile = $file[0]['prod_file'];
			if (file_exists("uploads/produtos/{$delFile}")):
		  		unlink("uploads/produtos/{$delFile}");
			endif;
			$this->Imagem();
		else:
			$this->Result = false;
		endif;
	}

	private function Cadastarar() {
		$Create = new Create();
		$Create->ExeCreate("produto", $this->Dados); 
		$this->Result = $Create->getResult();
	}

	private function Update() {
		$Update = new Update();
		$Update->ExeUpdate("produto", $this->Dados, "WHERE prod_id = {$this->Id} "); 
		$this->Result = $Update->getResult();
	}

    public function getResult() {
        return $this->Result;
    }

	private function CheckFileUpdate() {
        if (isset($this->Dados['prod_file'])):
      		$this->UpdateFile();
    	endif;
	}

	private function CheckProd() {
		$Read = new Read(); 
		$WHERE = (!empty($this->Id) ? "prod_id != {$this->Id} AND" : null);
		$Read->ExeRead("produto", "WHERE {$WHERE} prod_nome = '{$this->Dados['prod_nome']}'");
		if (!$Read->getResult()):
			$this->NomeProd = true;
		else:
			$this->NomeProd = false;
		endif; 
	}

	private function Url(){
		$this->Dados['prod_url'] = $this->Dados['prod_nome'];
		$str = $this->Dados['prod_url'];
	    $str = strtolower(utf8_decode($str)); $i=1;
	    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
	    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
	    while($i>0) $str = str_replace('--','-',$str,$i);
	    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
	    $this->Dados['prod_url'] = $str;
	}

	private function Imagem() {
		$extensao = strtolower(end(explode('.', $_FILES['prod_file']['name'])));
	 	$nome = $this->Dados['prod_url'].'-'.time() . '.' . $extensao;
		move_uploaded_file($this->Dados['prod_file'], "uploads/produtos/" . $nome);
		$this->Dados['prod_file'] = $nome;
  

	}




}