<?php
require_once('Conn.class.php'); 
class Read extends Conn {
	private $Select;
    private $Result;
    private $Read;
    private $Conn;

    public function ExeRead($Tabela, $Termos) {
        $this->Select = "SELECT * FROM {$Tabela} {$Termos}";
        $this->Execute();
    }


    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Read = $this->Conn->prepare($this->Select);
    }

    private function Execute() {
        $this->Connect();
        try { 
            $this->Read->execute();
            $this->Result = $this->Read->fetchAll();
        } catch (PDOException $e) {
            $this->Result = null;
            echo "<b>Erro ao ler: </b> {$e->getMessage()}";
        }
    }


    public function getResult() {
        return $this->Result;
    }


}