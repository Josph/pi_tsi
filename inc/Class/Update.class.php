<?php
require_once('Conn.class.php'); 
class Update extends Conn {
	private $UpdateSet;
    private $Result;
    private $Dados;
    private $Update;
    private $Conn;

    public function ExeUpdate($Tabela, $Dados, $Termos) {
        $this->Dados = $Dados;
        foreach ($this->Dados as $Key => $Value):
            $Places[] = $Key . ' = ' . "'". $Value . "'";
        endforeach;
        $Places = implode(', ', $Places);

        $this->UpdateSet = "UPDATE {$Tabela} SET {$Places} {$Termos}";
        $this->Execute();
    }


    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Update = $this->Conn->prepare($this->UpdateSet);
    }

    private function Execute() {
        $this->Connect();
        try { 
            $this->Update->execute();
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = null;
            echo "<b>Erro ao atualizar: </b> {$e->getMessage()}";
        }
    }


    public function getResult() {
        return $this->Result;
    }


}