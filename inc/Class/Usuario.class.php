<?php 
require_once('Create.class.php');
require_once('Read.class.php');
require_once('Update.class.php');
class Usuario{


  	private $Dados;
  	private $Result;
  	private $Id;
  	public $Email;



	public function ExeCreate(array $Dados) {
		$this->Dados = $Dados;
		$this->CheckEmail($Dados['email']);

		if ($this->Email):
			$this->Cadastarar();
		endif;
	}

	public function ExeUpdate(array $Dados) { 
		$this->Dados = $Dados;
		
		$this->Id = $this->Dados['usuario_id'];
    	unset($this->Dados['usuario_id']);

		$this->CheckEmail($Dados['email']);

		if($this->Email):
			$this->Update();
		endif;
	}

	private function Update() {
    	if (isset($this->Dados['senha'])):
        	$this->Dados['senha'] = md5($this->Dados['senha']);
    	endif;
		$Update = new Update();
		$Update->ExeUpdate("usuario", $this->Dados, "WHERE usuario_id = {$this->Id} "); 
		$this->Result = $Update->getResult();
	}

	private function Cadastarar() {
		$Create = new Create();
        $this->Dados['senha'] = md5($this->Dados['senha']);
		$Create->ExeCreate("usuario", $this->Dados);
		$this->Result = $Create->getResult();
	}

    public function getResult() {
        return $this->Result;
    }

	private function CheckEmail($email) {
		$Read = new Read(); 
		$WHERE = (!empty($this->Id) ? "usuario_id != {$this->Id} AND" : null);
		$Read->ExeRead("usuario", "WHERE {$WHERE} email = '{$email}'");
		
		if (!$Read->getResult()):
			$this->Email = true;
		else:
			$this->Email = false;
		endif;
	}


}