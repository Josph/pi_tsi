<?php
$Read = new Read;
$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent IS NULL ORDER BY cat_nome ASC");
if ($Read->getResult()):
  foreach ($Read->getResult() as $sessao):
    ?>
    <li class="dropdown">  
      <a href="<?=$url.$sessao['cat_url']; ?>" title="<?= $sessao['cat_nome'] ?>"><?= $sessao['cat_nome'] ?></a>                        
      <?php
      $Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent = {$sessao['cat_id']} ORDER BY cat_nome ASC");
      if ($Read->getResult()):
        ?>
        <ul class="sub-menu">
          <?php 
            foreach ($Read->getResult() as $cat): 
          ?>
            <li class="dropdown">
              <a href="<?=$url.$sessao['cat_url'].'/'.$cat['cat_url'];?>" title="<?= $cat['cat_nome'] ?>"><?= $cat['cat_nome'] ?></a>
            </li>
      <?php
            endforeach;
          $Read->ExeRead("produto", "WHERE prod_status = 1 AND cat_parent = {$sessao['cat_id']} ORDER BY prod_nome ASC");
          if ($Read->getResult()):
            foreach ($Read->getResult() as $prod): 
          ?>
            <li class="dropdown">
              <a href="<?=$url.$sessao['cat_url'].'/'.$prod['prod_url'];?>" title="<?= $prod['prod_nome'] ?>"><?= $prod['prod_nome'] ?></a>
            </li>
      <?php
            endforeach;
          endif;
      ?>
        </ul>
      <?php
      endif;
      ?>
    </li>
<?php 
  endforeach;
endif;
 ?>