<?php 
	$produto = end($URL);
	$Read->ExeRead("produto", "WHERE prod_status = 1 AND prod_url = '{$produto}' ORDER BY prod_nome ASC");
 ?>
<div class="produto">
<?php
	foreach ($Read->getResult() as $prod):
	extract($prod);
?>
	<h1><?=$prod_nome?></h1>

	<div class="caixa-produto">
		<div class="img-produto">
			<a href="<?=$url?>painel/uploads/produtos/<?=$prod_file?>" data-fancybox="group1" class="lightbox">
				<img src="<?=$url?>painel/uploads/produtos/<?=$prod_file?>" alt="<?=$prod_nome?>">
			</a>
		</div>
		<div class="desc-produto">
			<p><b>$R:</b> <?=$prod_preco?></p>
			<h2>Descrição</h2>
			<p><?=$prod_descricao?></p>
		<?php
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

          if (isset($post) && isset($post['comprar'])):
	          unset($post['comprar']);
			if (in_array("", $post)):
				echo '<script> pop_up_hacker("Erro: Preencha todos os campos."); </script>';
		    else:         
				require_once('Class/Pedido.class.php');

		        $Pedido = new Pedido();
				$Pedido->ExeCreate($post);

				if(!$Pedido->getResult()):
					echo '<script> pop_up_hacker("Erro: O sistema se comportou de maneira inesperada. Revise os dados e tente novamente."); </script>';
				else:
					echo '<script> pop_up_hacker("Aviso: Solicitação enviada com sucesso"); </script>';
				unset($post);
				endif;
	        endif;
          endif;
          ?> 
<form method="post">
	<input class="input-number" type="number" name="pedi_qtd" min="1" value="1">
	<input type="hidden" name="pedi_prod_id" value="<?=$prod_id?>">
	<input type="hidden" name="usuario_id" value="<?=$_SESSION['userlogin'][0]['usuario_id']?>">
	
	
<?php  
  if (!$login->CheckLogin()):
    unset($_SESSION['userlogin']);
?>
		<a class="btn-comprar" href="<?=$url?>login">Comprar</a>
<?php
  else:
    $nomeUser = $_SESSION['userlogin'][0]['nome'];
?>
		<input type="submit" name="comprar" value="Comprar" class="btn-comprar" >		

<?php
  endif;
 ?>

</form>
		</div>
	<hr>
	<div class="conteduto-prod">
		<?=$prod_conteudo?>
	</div>
	</div>

<?php
	endforeach;
 ?>
</div>