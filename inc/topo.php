<header class="topofixo">
<div class="container"> 
  <div class="logo"><a href="<?=$url?>"><img src="<?=$url?>imagens/logo.png" alt="Logo"></a></div>
  <nav id="menu">
    <ul>
      <li><a href="<?=$url?>">Home</a></li>
      <li><a href="<?=$url?>sobre-nos">Sobre Nós</a></li>
      <?php include 'inc/menu-top-inc.php' ?>
      <li><a href="<?=$url?>contato">Contato</a></li>
    </ul>
  </nav>
  <div class="menu-login">
<?php 
  session_start();
  $login = new Login();
  if (!$login->CheckLogin()):
    unset($_SESSION['userlogin']);
?>
    <a href="<?=$url?>login">Login</a>
<?php
  else:
    $nomeUser = $_SESSION['userlogin'][0]['nome'];
?>
    <a href="<?=$url?>painel/">Olá, <?=$nomeUser?></a>

<?php
  endif;
 ?>
  </div>
</div> 
</header>
<div class="menu-block"></div>
