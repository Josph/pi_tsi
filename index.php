<?php 
require_once('inc/geral.php');
$getURL = trim(strip_tags(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setURL = (empty($getURL) ? 'index' : $getURL);
$URL = explode('/', $setURL);

if ($URL[0] == "index"):
    require 'home.php';
else:

if (isset($URL[1]) && empty($URL[1])):
    header('location: ' . $url.$URL[0]); 

elseif(file_exists($URL[0] . ".php")):
    require $URL[0] . ".php";

else:
    header('location: ' . $url . '404');

endif;
endif;
