<?php
include 'inc/head.php';
?> 
</head>
<body>
	<main id="main-login">
	<div class="container-input">
	<?php 
		if(isset($_GET['cadastro'])):
			echo '<script> pop_up_hacker("Aviso: Usuario cadastrado com sucesso."); </script>';
		endif;

		$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		if(isset($post['entar'])):
			if(in_array("", $post)):
				echo '<script> pop_up_hacker("Erro: Preencha todos os campos."); </script>';
			else:
				require_once('inc/Class/Login.class.php');
				$login = new Login();
				$login->ExeLogin($post);
				if($login->getResult()):
					header('Location: painel/');
				else:
					echo '<script> pop_up_hacker("Erro: E-mail e/ou Senha incorreto."); </script>';
				endif;
			endif;
		endif;

	 ?>


	<svg x="0px" y="0px" viewBox="-10 -10 450.165 450.164">
	
		<path fill="none" d="M204.583,216.671c50.664,0,91.74-48.075,91.74-107.378c0-82.237-41.074-107.377-91.74-107.377
			c-50.668,0-91.74,25.14-91.74,107.377C112.844,168.596,153.916,216.671,204.583,216.671z"/>
		<path fill="none" d="M407.164,374.717L360.88,270.454c-2.117-4.771-5.836-8.728-10.465-11.138l-71.83-37.392
			c-1.584-0.823-3.502-0.663-4.926,0.415c-20.316,15.366-44.203,23.488-69.076,23.488c-24.877,0-48.762-8.122-69.078-23.488
			c-1.428-1.078-3.346-1.238-4.93-0.415L58.75,259.316c-4.631,2.41-8.346,6.365-10.465,11.138L2.001,374.717
			c-3.191,7.188-2.537,15.412,1.75,22.005c4.285,6.592,11.537,10.526,19.4,10.526h362.861c7.863,0,15.117-3.936,19.402-10.527
			C409.699,390.129,410.355,381.902,407.164,374.717z"/>
		</svg>
			<form method="POST">
				<input type="email" name="email" placeholder="Email">
				<input type="password" name="senha" placeholder="Senha">
				<input type="submit" name="entar" value="Entar">
			</form>
			<div class="btn">
				<a href="<?=$url?>"><b>Home</b></a> | 
				<a href="<?=$url?>cadastro"><b>Cadastrar-se</b></a>
			</div>
		</div>			
	</main>	

	<div id="pop-up-hacker"> 
		<div class="conteudo-pop-up">
		<button class="fechar">X</button>
			<p class="menssagem-pop-up">Erro: Usuário ou senha inválidos.</p>
		</div> 
	</div>	

</body>
</html>