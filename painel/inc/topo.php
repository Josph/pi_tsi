<header>
	<?php 
	$logout = filter_input(INPUT_GET, 'logout', FILTER_DEFAULT);
	if (isset($logout)):
  		unset($_SESSION['userlogin']);
  		header('Location: ../login');

	endif;
	 ?>
	<div class="menu-user">
		<a href="index.php"><img src="imagens/icone-usuario.jpg" alt="Foto Usuario"></a>
		<a href="index.php"><?=$_SESSION['userlogin'][0]['nome']?></a> | 

		<a href="?logout">Sair</a>
	</div>
	<nav class="menu-painel">
		<ul>
			<li><a href="../" target="_blanck"><i class="fas fa-home"></i> Home </a></li>
		<?php 
			if ($_SESSION['userlogin'][0]['usuario_id'] == 1):
		 ?>
			<li class="arcoddion"><a href="#">Categoria <i class="fas fa-list-ol"></i></a>
				<ul class="sub-menu-painel">
					<li><a href="?exe=categoria/index.php">Listar</a></li>
					<li><a href="?exe=categoria/cadastrar.php">Cadastrar</a></li>
				</ul>
			</li>
			<li class="arcoddion"><a href="#">Banner <i class="fas fa-desktop"></i></a>
				<ul class="sub-menu-painel">
					<li><a href="?exe=banner/index.php">Listar</a></li>
					<li><a href="?exe=banner/cadastrar.php">Cadastrar</a></li>
				</ul>
			</li>
			<li class="arcoddion"><a href="#">Produtos <i class="fas fa-shopping-cart"></i></a>
				<ul class="sub-menu-painel">
					<li><a href="?exe=produto/index.php">Listar</a></li>
					<li><a href="?exe=produto/cadastrar.php">Cadastrar</a></li>
				</ul>
			</li>
		<?php 
			endif;
		 ?>
			<li class="arcoddion"><a href="#">Pedidos <i class="fas fa-envelope"></i></a>
				<ul class="sub-menu-painel">
					<li><a href="?exe=pedido/index.php">Listar</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</header>