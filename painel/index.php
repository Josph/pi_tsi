<?php
require_once('../inc/Class/Login.class.php');
session_start();
$login = new Login();
if (!$login->CheckLogin()):
  unset($_SESSION['userlogin']);
  header('Location: ../login?exe=restrito');
endif;	
include 'inc/head.php';
?>
</head>
<body>
<?php include('inc/topo.php'); ?>

<main>
<?php 
$getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);

if (!empty($getexe)):
    if(!file_exists('sistema/'.$getexe)):
    	require_once('sistema/404.php');
    else:
    	require_once('sistema/'.$getexe);
    endif;
else:
	require_once('sistema/home.php');
endif;
?>
</main>

<script src="js/painel.js"></script> 

</body>