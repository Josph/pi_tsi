var arcoddion = document.querySelectorAll(".arcoddion>a");  

for (var i = 0; i < arcoddion.length; i++) {

  arcoddion[i].addEventListener("click", function(e) {
  	e.preventDefault();
    var  conteudo = this.nextElementSibling;
  	for(var i = 0; i < arcoddion.length; i++){
      arcoddion[i].nextElementSibling.style.maxHeight = null;
      if (arcoddion[i] != this){           
  		  arcoddion[i].className = "";
        }  		  		
  	}
    if (conteudo.style.maxHeight || this.classList.contains("active")){
      conteudo.style.maxHeight = null; 
    } else {
      conteudo.style.maxHeight = conteudo.scrollHeight + "px";
    } 
  	this.classList.toggle("active");

  });
}