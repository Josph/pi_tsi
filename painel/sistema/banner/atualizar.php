<?php  

	$get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
	$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

	
	if (isset($post['atualizar'])):
		unset($post['atualizar']);
		
		require_once('../inc/Class/Banner.class.php');
		$post['ban_id'] = $get;

		if (empty($_FILES['ban_file']['tmp_name'])):
			unset($_FILES['ban_file']);
		else:
			$post['ban_file'] = $_FILES['ban_file']['tmp_name'];			
		endif;
		
		if (in_array("", $post)):
			echo '<script>swal("Aviso!", "Todos os campos são obrigatórios.", "warning");</script>';
		else:
			$Banner = new Banner();
			$Banner->ExeUpdate($post);

			if(!$Banner->NomeBanner):
				echo '<script>swal("Erro!", "Banner já cadastrado.", "error");</script>';
			elseif(!$Banner->getResult()):
				echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
			else:
				echo '<script>swal("Tudo certo!", "Banner Atualizado com sucesso", "success");</script>';
				unset($post);
			endif;
		endif;
	endif;

 ?> 

<h1><i class="fas fa-desktop"></i> Atualizar Banner</h1>
<form method="POST" class="form-painel" enctype="multipart/form-data">
	<?php 
		$ReadBanner = new Read;
		$ReadBanner->ExeRead("banner", "WHERE ban_id = {$get}");
		foreach ($ReadBanner->getResult() as $key):
		extract($key);
			
	 ?>
	<div class="form-radio">
		<label>Publicar agora?</label>
		<div class="status-radio">
			<input type="radio" name="ban_status" id="ban_status1" value="1" <?php
				if (isset($post['ban_status']) && $post['ban_status'] == 1): echo 'checked="true"'; 
				elseif ($ban_status == 1): echo 'checked="true"';
				endif;
			?>>
			<label for="ban_status1">Sim</label> 

			<input type="radio" name="ban_status" id="ban_status2" value="2" <?php
				if (isset($post['ban_status']) && $post['ban_status'] == 2): echo 'checked="true"'; 
				elseif ($ban_status == 2): echo 'checked="true"';
				endif;
			?>>
			<label for="ban_status2">Não</label>

		</div>
	</div>

	<div class="form-file">
		<label for="ban_file">Banner: </label>
		<div class="img-atualizar">
			<img src="uploads/banner/<?=$ban_file?>" alt="<?=$ban_nome?>">
			<input type="file" name="ban_file" id="ban_file">
		</div>
	</div>
	<div class="form-input">
		<label for="ban_nome">Nome do Banner: </label>
		<input type="text" name="ban_nome" id="ban_nome" value="<?php 
			if(isset($post['ban_nome'])): echo$post['ban_nome'];
			else: echo $ban_nome;
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="ban_link">Link do Banner: </label>
		<input type="text" name="ban_link" id="ban_link" value="<?php 
			if(isset($post['ban_link'])): echo$post['ban_link'];
			else: echo $ban_link;
			endif;
		?>">
	</div>
	<div class="form-submit">
		<button type="submit" name="atualizar"><i class="fas fa-save"></i></button>
	</div>
	<?php 
		endforeach;
	 ?>
</form>