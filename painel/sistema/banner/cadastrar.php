<?php 
	$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
	
	if (isset($post['cadastrar'])):
		unset($post['cadastrar']);
		require_once('../inc/Class/Banner.class.php');
		$post['ban_file'] = $_FILES['ban_file']['tmp_name'];
		      
		if (in_array("", $post)):
			echo '<script>swal("Aviso!", "Todos os campos são obrigatórios.", "warning");</script>';
		else:
			$Banner = new Banner();
			$Banner->ExeCreate($post);

			if(!$Banner->NomeBanner):
				echo '<script>swal("Erro!", "Banner já cadastrado.", "error");</script>';
			elseif(!$Banner->getResult()):
				echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
			else: 
				echo '<script>swal("Tudo certo!", "Banner cadastrado com sucesso", "success");</script>';
				unset($post);
			endif;
		endif;
	endif;
 ?> 

<h1><i class="fas fa-desktop"></i> Banners</h1>
<form method="POST" class="form-painel" enctype="multipart/form-data">
	<div class="form-radio">
		<label>Publicar agora?</label>
		<div class="status-radio">
			<input type="radio" name="ban_status" id="ban_status1" value="1">
			<label for="ban_status1">Sim</label> 

			<input type="radio" name="ban_status" id="ban_status2" value="2">
			<label for="ban_status2">Não</label>

		</div>
	</div>

	<div class="form-file">
		<label for="ban_file">Banner: </label>
		<input type="file" name="ban_file" id="ban_file">
	</div>

	<div class="form-input">
		<label for="ban_nome">Nome do Banner: </label>
		<input type="text" name="ban_nome" id="ban_nome" value="<?php 
			if(isset($post['ban_nome'])): echo$post['ban_nome'];
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="ban_link">Link do Banner: </label>
		<input type="text" name="ban_link" id="ban_link" value="<?php 
			if(isset($post['ban_link'])): echo$post['ban_link'];
			endif;
		?>">
	</div>
	<div class="form-submit">
		<button type="submit" name="cadastrar"><i class="fas fa-save"></i></button>
	</div>
	
</form>