<h1><i class="fas fa-desktop"></i> Listagem dos Banners</h1>

<div class="container-painel">
	<table class="table-listar">
		<thead>
			<tr>
			<th>#</th>
			<th>Imagem</th>
			<th>Nome</th>
			<th>Link</th>
			<th>Status</th>
			<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$ReadBanner = new Read;
				$ReadBanner->ExeRead("banner", "ORDER BY ban_nome ASC");
				if ($ReadBanner->getResult()):
					foreach ($ReadBanner->getResult() as $key):
					extract($key);
					?>					
					<tr>
						<td><?=$ban_id?></td>
						<td class="td-img"><img src="uploads/banner/<?=$ban_file?>" alt="<?=$ban_nome?>"></td>
						<td><?=$ban_nome?></td>
						<td><?=$ban_link?></td>
						<td>
							<?php 
								if ($ban_status == 1):
									echo "<b style=\"color: green;\">Online</b>";
								else:
									echo "<b style=\"color: red;\">Offline</b>";
								endif 
							?>
	
						</td>                                                               
						<td>

							<a  href="index.php?exe=banner/atualizar.php&id=<?= $ban_id; ?>" class="acoes-editar"><i class="fas fa-pencil-alt"></i></a>

							<a  href="index.php?exe=banner/index.php&del=<?=$ban_id; ?>" class="acoes-excluir"><i class="fas fa-trash"></i></a>
							
						</td>                                                                
					</tr>
					<?php
					endforeach;
				else:
					echo '<script>swal("Ops!!", "Nenhum Banner foi encontrado.", "warning");</script>';
					echo "<p><b>Nenhum Banner foi encontrado</b></p>";					
				endif;
			 ?>
		</tbody>
	</table>
</div>
<?php 
	$del = filter_input(INPUT_GET, 'del', FILTER_DEFAULT);
	if (isset($del) && !empty($del)):
  		require_once('../inc/Class/Delete.class.php');
  		$Delete = new Delete();
  		$Delete->ExeDelete("banner", "WHERE ban_id = {$del}");
  		if ($Delete->getResult()): 
			echo '<script>swal("Tudo certo!", "Banner deletado com sucesso", "success");</script>';
  			header('Location: ?exe=banner/index.php');

  		else:
			echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
  			header('Location: ?exe=banner/index.php');
			

  		endif;

	endif;
 ?>