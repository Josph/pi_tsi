<?php  

	$get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
	$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

	
	if (isset($post['atualizar'])):
		unset($post['atualizar']); 
		
		require_once('../inc/Class/Categoria.class.php');
		$post['cat_id'] = $get;

		if (empty($_FILES['cat_file']['tmp_name'])):
			unset($_FILES['cat_file']);
		else:
			$post['cat_file'] = $_FILES['cat_file']['tmp_name'];			
		endif;

		if (empty($post['cat_parent'])):
			unset($post['cat_parent']);
		endif;
		
		if (in_array("", $post)):
			echo '<script>swal("Aviso!", "Todos os campos são obrigatórios.", "warning");</script>';
		else:
			$Categoria = new Categoria();
			$Categoria->ExeUpdate($post);
 
			if(!$Categoria->NomeCat):
				echo '<script>swal("Erro!", "Categoria já cadastrada.", "error");</script>';
			elseif(!$Categoria->getResult()):
				echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
			else:
				echo '<script>swal("Tudo certo!", "Categoria Atualizada com sucesso", "success");</script>';
				unset($post);
			endif;
		endif;
	endif;

 ?> 

<h1><i class="fas fa-list-ol"></i> Atualizar Categoria</h1>
<form method="POST" class="form-painel" enctype="multipart/form-data">
	<?php 
		$ReadCategoria = new Read;
		$ReadCategoria->ExeRead("categoria", "WHERE cat_id = {$get}");
		foreach ($ReadCategoria->getResult() as $key):
		extract($key);
			
	 ?>
	<div class="form-radio">
		<label>Publicar agora?</label>
		<div class="status-radio">
			<input type="radio" name="cat_status" id="cat_status1" value="1" <?php
				if (isset($post['cat_status']) && $post['cat_status'] == 1): echo 'checked="true"'; 
				elseif ($cat_status == 1): echo 'checked="true"';
				endif;
			?>>
			<label for="cat_status1">Sim</label> 

			<input type="radio" name="cat_status" id="cat_status2" value="2" <?php
				if (isset($post['cat_status']) && $post['cat_status'] == 2): echo 'checked="true"'; 
				elseif ($cat_status == 2): echo 'checked="true"';
				endif;
			?>>
			<label for="cat_status2">Não</label>

		</div>
	</div>

	<div class="form-select">
		<label for="cat_nome">Sessão ou categoria: </label>
		<select name="cat_parent">
			<?php
			$ReadCategoria = new Read;
			$ReadCategoria->ExeRead("categoria", "WHERE cat_parent IS NULL ORDER BY cat_url ASC");
			?>
			<option value="">Isto é uma sessão!</option>
			    <?php
			        if ($ReadCategoria->getResult()):
			        	foreach ($ReadCategoria->getResult() as $key):
			    ?>
						    <option value="<?= $key['cat_id'] ?>" <?php
						    	if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id'] || $key['cat_id'] == $cat_parent): echo 'selected="selected"';
						    	endif;
				    		?>>» <?= $key['cat_nome']; ?></option>
			            <?php
							$ReadCategoria->ExeRead("categoria", "WHERE cat_parent = {$key['cat_id']} ORDER BY cat_url ASC");
                          if ($ReadCategoria->getResult()):
                            foreach ($ReadCategoria->getResult() as $keys):
                              ?>
                              <option value="<?= $keys['cat_id']; ?>" disabled="disabled" style="color: #73879c; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow; <?= $keys['cat_nome']; ?></option>
                              <?php
                            endforeach;
                          endif;
			  			endforeach;
					endif;
			?> 
		</select>
	</div>
	<div class="form-file">
		<label for="cat_file">Capa da Categoria: </label>
		<div class="img-atualizar">
			<img src="uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>">
			<input type="file" name="cat_file" id="cat_file">
		</div>
	</div>
	<div class="form-input">
		<label for="cat_nome">Nome da Categoria: </label>
		<input type="text" name="cat_nome" id="cat_nome" value="<?php 
			if(isset($post['cat_nome'])): echo $post['cat_nome'];
			else: echo $cat_nome;
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="cat_descricao">Descrição do Categoria: </label>
		<input type="text" name="cat_descricao" id="cat_descricao" value="<?php 
			if(isset($post['cat_descricao'])): echo $post['cat_descricao'];
			else: echo $cat_descricao;
			endif;
		?>">
	</div>
	
	<div class="form-text">
		<label for="cat_conteudo">Conteudo: </label>
		<textarea name="cat_conteudo" id="cat_conteudo" rows="20"><?php
	        if (isset($post['cat_conteudo'])): echo $post['cat_conteudo'];
	        else: echo $cat_conteudo;
	        endif;
        ?></textarea>
	</div>
	<div class="form-submit">
		<button type="submit" name="atualizar"><i class="fas fa-save"></i></button>
	</div>
	<?php 
		endforeach;
	 ?>
</form>