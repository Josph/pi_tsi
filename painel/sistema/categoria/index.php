<h1><i class="fas fa-list-ol"></i> Listagem das Categorias</h1>

<div class="container-painel">
	<table class="table-listar">
		<thead>
			<tr>
			<th>#</th>
			<th>Imagem</th>
			<th>Nome</th>
			<th>Descrição</th>
			<th>Status</th>
			<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$ReadCategoria = new Read;
				$ReadCategoria->ExeRead("categoria", "ORDER BY cat_nome ASC");
				if ($ReadCategoria->getResult()):
					foreach ($ReadCategoria->getResult() as $key):
					extract($key);
					?>					
					<tr>
						<td><?=$cat_id?></td>
						<td class="td-img"><img src="uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>"></td>
						<td><?=$cat_nome?></td>
						<td><?=$cat_descricao?></td>
						<td>
							<?php 
								if ($cat_status == 1):
									echo "<b style=\"color: green;\">Online</b>";
								else:
									echo "<b style=\"color: red;\">Offline</b>";
								endif 
							?>
	
						</td>                                                               
						<td>

							<a  href="index.php?exe=categoria/atualizar.php&id=<?= $cat_id; ?>" class="acoes-editar"><i class="fas fa-pencil-alt"></i></a>

							<a  href="index.php?exe=categoria/index.php&del=<?=$cat_id; ?>" class="acoes-excluir"><i class="fas fa-trash"></i></a>

						</td>                                                                
					</tr>
					<?php
					endforeach;
				else:
					echo '<script>swal("Ops!!", "Nenhuma Categoria foi encontrada.", "warning");</script>';
					echo "<p><b>Nenhuma Categoria foi encontrada</b></p>";					
				endif;
			 ?>
		</tbody>
	</table>
</div>
<?php 
	$del = filter_input(INPUT_GET, 'del', FILTER_DEFAULT);
	if (isset($del) && !empty($del)):
  		require_once('../inc/Class/Delete.class.php');
  		$Delete = new Delete();
  		$Delete->ExeDelete("categoria", "WHERE cat_id = {$del}");
  		if ($Delete->getResult()):
			echo '<script>swal("Tudo certo!", "Categoria deletado com sucesso", "success");</script>';
  			header('Location: ?exe=categoria/index.php');

  		else:
			echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
  			header('Location: ?exe=categoria/index.php');
			

  		endif;

	endif;
 ?>