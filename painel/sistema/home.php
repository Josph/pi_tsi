<?php  

	$get = $_SESSION['userlogin'][0]['usuario_id'];
	$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
	
	
	if (isset($post['atualizar'])):
		unset($post['atualizar']);
		
		require_once('../inc/Class/Usuario.class.php');
		$post['usuario_id'] = $get;

		if (empty($_FILES['file']['tmp_name'])):
			unset($_FILES['file']);
		else:
			$post['file'] = $_FILES['prod_file']['tmp_name'];			
		endif;

		if (empty($post['senha'])):
			unset($post['senha']);			
		endif;

		if (in_array("", $post)):
			echo '<script>swal("Aviso!", "Todos os campos são obrigatórios.", "warning");</script>';
		else:
			$Usuario = new Usuario();
			$Usuario->ExeUpdate($post);
 
			if(!$Usuario->Email):
				echo '<script>swal("Erro!", "Usuario já cadastrado.", "error");</script>';
			elseif(!$Usuario->getResult()):
				echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
			else:
				echo '<script>swal("Tudo certo!", "Usuario Atualizado com sucesso", "success");</script>';
				unset($post);
			endif;
		endif;
	endif;

 ?> 
 
<h1><i class="fas fa-user"></i> Atualizar Usuario</h1>
<form method="POST" class="form-painel" enctype="multipart/form-data">
	<?php 
		$ReadUsuario = new Read;
		$ReadUsuario->ExeRead("usuario", "WHERE usuario_id = {$get}");
		foreach ($ReadUsuario->getResult() as $key):
		extract($key);
			
	 ?>
<!-- 	<div class="form-file">
		<label for="prod_file">Capa do Usuario: </label>
		<div class="img-atualizar">
			<img src="uploads/usuario/<?=$prod_file?>" alt="<?=$prod_nome?>">
			<input type="file" name="prod_file" id="prod_file">
		</div>
	</div>
 -->
	<div class="form-input">
		<label for="nome">Nome do Usuario: </label>
		<input type="text" name="nome" id="nome" value="<?php 
			if(isset($post['nome'])): echo$post['nome'];
			else: echo $nome;
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="email">Email do Usuario: </label>
		<input type="text" name="email" id="email" value="<?php 
			if(isset($post['email'])): echo$post['email'];
			else: echo $email;
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="senha">Senha do Usuario: </label>
		<input type="password" name="senha" id="senha">
	</div>
	<div class="form-input">
		<label for="palavra">Palavra <br> (Recuperar Senha): </label>
		<input type="text" name="palavra" id="palavra" value="<?php 
			if(isset($post['palavra'])): echo$post['palavra'];
			else: echo $palavra;
			endif;
		?>">
	</div>
	<div class="form-submit">
		<button type="submit" name="atualizar"><i class="fas fa-save"></i></button>
	</div>
	<?php 
		endforeach;
	 ?>
</form>