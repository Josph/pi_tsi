<h1><i class="fas fa-shopping-cart"></i> Listagem dos Pedido</h1>

<div class="container-painel">
	<table class="table-listar">
		<thead>
			<tr>
			<th>Data</th>
			<th>Imagem</th>
			<th>Nome</th>
			<th>Descrição</th>
			<th>Quantidade/Valor</th>
			<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$ReadPedido = new Read;
				$ReadPedido->ExeRead("pedido", "JOIN produto ON pedi_prod_id = prod_id WHERE usuario_id = {$_SESSION['userlogin'][0]['usuario_id']}");
 					if ($ReadPedido->getResult()):
					foreach ($ReadPedido->getResult() as $key):
					extract($key);
					?>					
					<tr>
						<td><?=str_replace("-", "/", $pedi_data)?></td>
						<td class="td-img"><img src="uploads/produtos/<?=$prod_file?>" alt="<?=$prod_nome?>"></td>
						<td><?=$prod_nome?></td>
						<td><?=$prod_descricao?></td>
						<td>
							<p>Quandidade: <?=$pedi_qtd?></p>
							<p>Valor: <?=($prod_preco * $pedi_qtd)?></p>
	
						</td>                                                               
						<td>
							
							<a  href="index.php?exe=pedido/index.php&del=<?=$pedi_id; ?>" class="acoes-excluir"><i class="fas fa-trash"></i></a>
						</td>                                                                
					</tr>
					<?php
					endforeach;
				else:
					echo '<script>swal("Ops!!", "Nenhum produto foi encontrado.", "warning");</script>';
					echo "<p><b>Nenhum pedido foi encontrado</b></p>";					
				endif;
			 ?>
		</tbody>
	</table>
</div>
<?php 
	$del = filter_input(INPUT_GET, 'del', FILTER_DEFAULT);
	if (isset($del) && !empty($del)):
  		require_once('../inc/Class/Delete.class.php');
  		$Delete = new Delete();
  		$Delete->ExeDelete("pedido", "WHERE pedi_id = {$del}");
  		if ($Delete->getResult()):
			echo '<script>swal("Tudo certo!", "Produto deletado com sucesso", "success");</script>';
  			header('Location: ?exe=pedido/index.php');

  		else:
			echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
  			header('Location: ?exe=pedido/index.php'); 
			

  		endif;

	endif;
 ?>