<?php 
	$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
	
	if (isset($post['cadastrar'])):
		unset($post['cadastrar']);
		require_once('../inc/Class/Produto.class.php');
		$post['prod_file'] = $_FILES['prod_file']['tmp_name'];
		      
		if (in_array("", $post)):
			echo '<script>swal("Aviso!", "Todos os campos são obrigatórios.", "warning");</script>';
		else:
			$Produto = new Produto();
			$Produto->ExeCreate($post);

			if(!$Produto->NomeProd):
				echo '<script>swal("Erro!", "Produto já cadastrado.", "error");</script>';
			elseif(!$Produto->getResult()):
				echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
			else:
				echo '<script>swal("Tudo certo!", "Produto cadastrado com sucesso", "success");</script>';
				unset($post);
			endif;
		endif;
	endif;
 ?> 

<h1><i class="fas fa-shopping-cart"></i> Produtos</h1>
<form method="POST" class="form-painel" enctype="multipart/form-data">
	<div class="form-radio">
		<label>Publicar agora?</label>
		<div class="status-radio">
			<input type="radio" name="prod_status" id="prod_status1" value="1">
			<label for="prod_status1">Sim</label> 

			<input type="radio" name="prod_status" id="prod_status2" value="2">
			<label for="prod_status2">Não</label>

		</div>
	</div>
	<div class="form-select">
		<label for="cat_nome">Categoria: </label>
		<select name="cat_parent">
			<?php
			$ReadCategoria = new Read;
			$ReadCategoria->ExeRead("categoria", "WHERE cat_parent IS NULL ORDER BY cat_url ASC");
			?>
			    <?php
			        if ($ReadCategoria->getResult()):
						foreach ($ReadCategoria->getResult() as $key):
						extract($key);
			    ?>
						    <option value="<?= $key['cat_id'] ?>" <?php
						    	if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id'] || $key['cat_id'] == $cat_parent): echo 'selected="selected"';
						    	endif;
				    		?>>» <?= $key['cat_nome']; ?></option>
			            <?php
							$ReadCategoria->ExeRead("categoria", "WHERE cat_parent = {$key['cat_id']} ORDER BY cat_url ASC");
                          if ($ReadCategoria->getResult()):
                            foreach ($ReadCategoria->getResult() as $keys):
                              ?>
                              <option value="<?= $keys['cat_id']; ?>" <?php
						    	if (isset($post['cat_parent']) && $post['cat_parent'] == $keys['cat_id'] || $keys['cat_id'] == $cat_parent): echo 'selected="selected"';
						    	endif;
						    	?>
						    	style="color: #73879c; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow; <?= $keys['cat_nome']; ?></option>
                              <?php
                            endforeach;
                          endif;
			  			endforeach;
					endif;
			?> 
		</select>
	</div>
	<div class="form-file">
		<label for="prod_file">Capa do produto: </label>
		<input type="file" name="prod_file" id="prod_file">
	</div>

	<div class="form-input">
		<label for="prod_nome">Nome do produto: </label>
		<input type="text" name="prod_nome" id="prod_nome" value="<?php 
			if(isset($post['prod_nome'])): echo$post['prod_nome'];
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="prod_preco">Preço do produto: </label>
		<input type="text" name="prod_preco" id="prod_preco" value="<?php 
			if(isset($post['prod_preco'])): echo$post['prod_preco'];
			endif;
		?>">
	</div>
	<div class="form-input">
		<label for="prod_descricao">Descrição do produto: </label>
		<input type="text" name="prod_descricao" id="prod_descricao" value="<?php 
			if(isset($post['prod_descricao'])): echo$post['prod_descricao'];
			endif;
		?>">
	</div>
	
	<div class="form-text">
		<label for="prod_conteudo">Conteudo: </label>
		<textarea name="prod_conteudo" id="prod_conteudo" rows="25"><?php
	        if (isset($post['prod_conteudo'])): echo $post['prod_conteudo'];
	        endif;
        ?></textarea>
	</div>
	<div class="form-submit">
		<button type="submit" name="cadastrar"><i class="fas fa-save"></i></button>
	</div>
	
</form>