<h1><i class="fas fa-shopping-cart"></i> Listagem dos Produtos</h1>

<div class="container-painel">
	<table class="table-listar">
		<thead>
			<tr>
			<th>#</th>
			<th>Imagem</th>
			<th>Nome</th>
			<th>Descrição</th>
			<th>Status</th>
			<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$ReadProdutos = new Read;
				$ReadProdutos->ExeRead("produto", "ORDER BY prod_nome ASC");
				if ($ReadProdutos->getResult()):
					foreach ($ReadProdutos->getResult() as $key):
					extract($key);
					?>					
					<tr>
						<td><?=$prod_id?></td>
						<td class="td-img"><img src="uploads/produtos/<?=$prod_file?>" alt="<?=$prod_nome?>"></td>
						<td><?=$prod_nome?></td>
						<td><?=$prod_descricao?></td>
						<td>
							<?php 
								if ($prod_status == 1):
									echo "<b style=\"color: green;\">Online</b>";
								else:
									echo "<b style=\"color: red;\">Offline</b>";
								endif 
							?>
	
						</td>                                                               
						<td>
							
							<a  href="index.php?exe=produto/atualizar.php&id=<?= $prod_id; ?>" class="acoes-editar"><i class="fas fa-pencil-alt"></i></a>

							<a  href="index.php?exe=produto/index.php&del=<?=$prod_id; ?>" class="acoes-excluir"><i class="fas fa-trash"></i></a>
						</td>                                                                
					</tr>
					<?php
					endforeach;
				else:
					echo '<script>swal("Ops!!", "Nenhum produto foi encontrado.", "warning");</script>';
					echo "<p><b>Nenhum produto foi encontrado</b></p>";					
				endif;
			 ?>
		</tbody>
	</table>
</div>
<?php 
	$del = filter_input(INPUT_GET, 'del', FILTER_DEFAULT);
	if (isset($del) && !empty($del)):
  		require_once('../inc/Class/Delete.class.php');
  		$Delete = new Delete();
  		$Delete->ExeDelete("produto", "WHERE prod_id = {$del}");
  		if ($Delete->getResult()):
			echo '<script>swal("Tudo certo!", "Produto deletado com sucesso", "success");</script>';
  			header('Location: ?exe=produto/index.php');

  		else:
			echo '<script>swal("Erro!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");</script>';
  			header('Location: ?exe=produto/index.php');
			

  		endif;

	endif;
 ?>