<?php 

include 'inc/head.php';

 ?>
</head>
<body>
<?php 

include 'inc/topo.php'; 
include 'inc/fancy.php'; 

$categ = end($URL);
$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_url = '{$categ}' ORDER BY cat_nome ASC");
$catPai = array_shift($URL);
?>

<main class="container">
	<section>
		<?php
		if (!$Read->getResult()):
			include 'inc/produto-inc.php';
		else:
		foreach ($Read->getResult() as $sessao):
		extract($sessao);
		?>
		<h1><?=$cat_nome?></h1>
		<?=$cat_conteudo?>
		<?php
			$Read->ExeRead("categoria", "WHERE cat_status = 1 AND cat_parent = {$cat_id} ORDER BY cat_nome ASC");
			if (!$Read->getResult()):
				$Read->ExeRead("produto", "WHERE prod_status = 1 AND cat_parent = {$cat_id} ORDER BY prod_nome ASC");
			?>
			<ul class="thumbnails">
			<?php
				foreach ($Read->getResult() as $prod):
				extract($prod);
				?>
				<li>
					<a href="<?=$url.$catPai.'/'.$categ.'/'.$prod_url?>" title="<?=$prod_nome?>">
					<img src="<?=$url?>painel/uploads/produtos/<?=$prod_file?>" alt="<?=$prod_nome?>" title="<?=$prod_nome?>"/></a>
					<h2><a href="<?=$url.$catPai.'/'.$categ.'/'.$prod_url?>" title="<?=$prod_nome?>"><?=$prod_nome?></a></h2>
				</li>
				<?php
				endforeach;
			?>
			</ul>
			<?php
			else:
			?>
			<ul class="thumbnails">
			<?php
				foreach ($Read->getResult() as $cat):
				extract($cat);
				?>

				<li> 
					<a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>">
					<img src="<?=$url?>painel/uploads/categoria/<?=$cat_file?>" alt="<?=$cat_nome?>" title="<?=$cat_nome?>"/></a>
					<h2><a href="<?=$url.$categ.'/'.$cat_url?>" title="<?=$cat_nome?>"><?=$cat_nome?></a></h2>
				</li>
				<?php
				endforeach;
				?>
			<?php 
				$Read->ExeRead("produto", "WHERE prod_status = 1 AND cat_parent = {$sessao['cat_id']} ORDER BY prod_nome ASC");
				if($Read->getResult()):	
			 ?>
			<?php
				foreach ($Read->getResult() as $prod):
				extract($prod);
				?>
				<li>
					<a href="<?=$url.$categ.'/'.$prod_url?>" title="<?=$prod_nome?>">
					<img src="<?=$url?>painel/uploads/produtos/<?=$prod_file?>" alt="<?=$prod_nome?>" title="<?=$prod_nome?>"/></a>
					<h2><a href="<?=$url.$categ.'/'.$prod_url?>" title="<?=$prod_nome?>"><?=$prod_nome?></a></h2>
				</li>
				<?php
				endforeach;
			?>
				<?php
			endif;
			?>
			</ul>
			<?php
			endif;
		endforeach;		
		endif;
		
		?>
	</section>
</main>
<?php 
	include 'inc/footer.php';
 ?>

</body>